module Timestamp
  # Takes an url as an input and appends the current time to it
  def timestamp(input_url)
    "#{input_url}?#{Time.now.to_i}"
  end
end

Liquid::Template.register_filter(Timestamp)
