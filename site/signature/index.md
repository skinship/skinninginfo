---
layout: master
highlight: signature
description: Get our skinship forum signature here!
title: skinship | Signature
---

# Our forum signatures!

That's right, if you love this server a lot you can show your support with a cool themed banner when you post on the osu! forums. <br>
We are grateful for everyone who decides to support us this way!

Get the full size one:
<img src="/img/signature.png">

`[centre][url=https://discord.skinship.xyz/][img]https://skinship.xyz/img/signature.png[/img][/url][/centre]`

Or a half-sized one:
<img src="/img/signature_half.png">

`[centre][url=https://discord.skinship.xyz/][img]https://skinship.xyz/img/signature_half.png[/img][/url][/centre]`

Or if you need the space on your signature, the quarter-sized one:
<img src="/img/signature_quarter.png">

`[centre][url=https://discord.skinship.xyz/][img]https://skinship.xyz/img/signature_quarter.png[/img][/url][/centre]`
