import ts from "@wessberg/rollup-plugin-ts";

export default {
    output: {
        format: 'cjs'
    },
    plugins: [
        ts({
        })
    ]
};