export interface BBCodeEditor {
    getBBCodeData(): BBCodeData
}

export class BBCodeData {}

export class TextAreaData extends BBCodeData {
    constructor(readonly value: string) {
        super()
    }
}

export class ListRowData extends TextAreaData {
    constructor(value: string) {
        super(value)
    }
}

export class ListData extends BBCodeData {
    constructor(readonly type: "numbered" | "bullet", readonly items: (ListData | ListRowData)[]) {
        super()
    }
}

export class ImageMapData extends BBCodeData {
    constructor(readonly url: string, readonly areas: ImageMapAreaData[]) {
        super()
    }
}

export class ImageMapAreaData extends BBCodeData {
    constructor(readonly x: number, readonly y: number, readonly width: number, readonly height: number, readonly title: string, readonly url: string) {
        super()
    }
}
