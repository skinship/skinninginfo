import { ListRowData } from "../../types/bbcode"
import { BBCodeElementWrapper } from "./BBCodeElementWrapper"
import { TextArea } from "./TextArea"

export class ListElementEditor extends TextArea {
    constructor(wrapper: BBCodeElementWrapper) {
        super(wrapper)

        this.classList.add("bbcode-list-editor__entry")
    }

    getBBCodeData(): ListRowData {
        return new ListRowData(this.value)
    }
}
customElements.define("list-editor-element", ListElementEditor, { extends: "textarea" })
