import { createElement } from "../../../typescript-lib/src/shared/dom"
import { BBCodeData, BBCodeEditor, ImageMapData } from "../../types/bbcode"
import { ImageMapArea } from "./ImageMapArea"

export class ImageMapEditor extends HTMLElement implements BBCodeEditor {
    private imgElement = createElement("img", {
        className: "image-map-editor__image",
        attributes: {
            draggable: false,
        },
    })

    private currentArea: ImageMapArea | null = null
    private url: string
    constructor(data: ImageMapData) {
        super()

        this.url = data.url
        this.imgElement.src = this.url

        data.areas.forEach((area) => {
            this.append(new ImageMapArea(this, null, area))
        })

        this.classList.add("image-map-editor")

        this.append(this.imgElement)

        let mouseDown = false
        this.addEventListener("mousedown", (e) => {
            if (e.target != this.imgElement) {
                mouseDown = false
                return
            }
            // check for open popups
            if (document.querySelector(".image-map-editor__area-form.visible") != null) {
                return
            }
            mouseDown = true
            this.currentArea = new ImageMapArea(this, e)
            this.append(this.currentArea)
            this.currentArea.finishInit()
        })
        this.addEventListener("mousemove", (e) => {
            if (mouseDown) {
                this.currentArea?.expand(e)
            }
        })
        this.addEventListener("mouseup", (e) => {
            mouseDown = false
            this.currentArea?.classList.add("placement-finished")
        })
        this.addEventListener("mouseleave", (e) => {
            mouseDown = false
            this.currentArea?.classList.add("placement-finished")
        })
    }

    getBBCodeData(): BBCodeData {
        const data = new ImageMapData(this.url.trim(), [])

        this.querySelectorAll<ImageMapArea>(".image-map-editor__area").forEach((area) => {
            data.areas.push(area.getBBCodeData())
        })

        return data
    }
}

customElements.define("imagemap-editor", ImageMapEditor)
