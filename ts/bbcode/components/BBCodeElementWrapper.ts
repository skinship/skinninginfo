import { createElement, createIcon } from "../../../typescript-lib/src/shared/dom"
import { BBCodeEditor } from "../../types/bbcode"

export class BBCodeElementWrapper extends HTMLElement {
    private settingsPopup = createElement("div", {
        className: "bbcode-element-wrapper__settings-popup",
    })
    private bbCodeElement: BBCodeEditor | null = null

    constructor(childName: string) {
        super()

        this.classList.add("bbcode-element-wrapper")

        const settingsButton = createElement("div", {
            className: "bbcode-element-wrapper__settings-button",
            attributes: {
                title: "settings",
                onclick: () => {
                    this.toggleSettingsPopup()
                },
            },
            children: [createIcon("setting")],
        })
        this.append(
            this.settingsPopup,
            settingsButton,
            createElement("span", {
                className: "bbcode-element-wrapper__label",
                attributes: {
                    innerText: childName,
                },
            })
        )

        this.addPopupElement(
            createElement("button", {
                className: "bbcode-element-wrapper__settings-popup-button bbcode-element-wrapper__settings-popup-button--delete button",
                attributes: {
                    innerText: "delete",
                    onclick: () => {
                        this.remove()
                    },
                },
            })
        )

        document.addEventListener("click", (e) => {
            const target = e.target as Node

            if (target != null) {
                if (settingsButton.contains(target)) {
                    // let button handle it
                    return
                }

                // if neither the popup or the button are the source
                if (!this.settingsPopup.contains(target)) {
                    this.settingsPopup.classList.remove("visible")
                }
            }
        })
    }

    public registerChild(element: HTMLElement) {
        this.append(element)

        if ((element as any)["getBBCodeData"] instanceof Function) {
            this.bbCodeElement = element as unknown as BBCodeEditor
        }
    }

    public getChild(): BBCodeEditor {
        if (this.bbCodeElement == null) {
            throw new Error("")
        } else {
            return this.bbCodeElement
        }
    }

    private toggleSettingsPopup() {
        this.settingsPopup.classList.toggle("visible")
    }

    public addPopupElement(element: HTMLElement) {
        this.settingsPopup.append(element)
    }
}

customElements.define("bbcode-element-wrapper", BBCodeElementWrapper)
