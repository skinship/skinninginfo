import { createElement } from "../../../typescript-lib/src/shared/dom"
import { ImageMapAreaData } from "../../types/bbcode"
import { ImageMapArea } from "./ImageMapArea"

export class ImageMapAreaForm extends HTMLElement {
    private tooltipElement = createElement("input", {
        attributes: {
            placeholder: "tooltip (optional)",
            title: "tooltip (optional)",
            type: "text",
            value: "",
        },
    })
    private urlElement = createElement("input", {
        attributes: {
            placeholder: "url (optional)",
            title: "url (optional)",
            type: "text",
            value: "",
        },
    })

    private xElement = this.createInputField()
    private yElement = this.createInputField()
    private wElement = this.createInputField()
    private hElement = this.createInputField()
    constructor(parent: ImageMapArea, data?: ImageMapAreaData) {
        super()

        this.classList.add("image-map-editor__area-form")

        if (data) {
            // w, h, x, y are set by the controlling area
            this.urlElement.value = data.url
            this.tooltipElement.value = data.title
        }

        this.append(
            this.tooltipElement,
            this.urlElement,
            createElement("button", {
                className: "image-map-editor__area-form-button image-map-editor__area-form-button--delete button",
                attributes: {
                    innerText: "delete",
                    onclick: () => {
                        parent.remove()
                        this.remove()
                    },
                },
            }),
            createElement("button", {
                className: "image-map-editor__area-form-button image-map-editor__area-form-button--width button button--primary",
                attributes: {
                    innerText: "← full width →",
                    onclick: () => {
                        parent.setWidth(100.0)
                    },
                },
            }),
            createElement("button", {
                className: "image-map-editor__area-form-button image-map-editor__area-form-button--height button button--primary",
                attributes: {
                    innerText: "↕ full height ↕",
                    onclick: () => {
                        parent.setHeight(100.0)
                    },
                },
            }),
            this.createField(this.xElement, "x"),
            this.createField(this.yElement, "y"),
            this.createField(this.wElement, "width"),
            this.createField(this.hElement, "height")
        )
    }

    public getXField() {
        return this.xElement
    }
    public getYField() {
        return this.yElement
    }
    public getWidthField() {
        return this.wElement
    }
    public getHeightField() {
        return this.hElement
    }

    private createInputField() {
        return createElement("input", {
            attributes: {
                type: "number",
                value: "0.0",
                max: "100.0",
                min: "-100.0",
                step: "0.1",
            },
        })
    }

    private createField(input: HTMLElement, label: string) {
        return createElement("label", {
            children: [input],
            attributes: {
                innerText: label,
            },
        })
    }

    public getTitle() {
        return this.tooltipElement.value
    }

    public getURL() {
        const value = this.urlElement.value.trim()
        if(value == ""){
            return "" // dont attempt to https check if the value is empty
        }
        let urlObj: URL | string
        try {
            urlObj = new URL(value)
        } catch (e) {
            // try to catch URLs that are missing a scheme by forcefully appending https, if it fails invalidate this field
            try {
                urlObj = new URL("https://" + value)
            } catch (e) {
                urlObj = ""
            }
        }
        return urlObj.toString()
    }
}

customElements.define("imagemap-editor-area-controls", ImageMapAreaForm)
