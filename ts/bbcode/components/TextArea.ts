import { createElement } from "../../../typescript-lib/src/shared/dom"
import { BBCodeEditor, TextAreaData } from "../../types/bbcode"
import { BBCodeElementWrapper } from "./BBCodeElementWrapper"

export class TextArea extends HTMLTextAreaElement implements BBCodeEditor {
    private wrapper

    constructor(wrapper: BBCodeElementWrapper) {
        super()
        this.wrapper = wrapper

        this.classList.add("bbcode-textarea-editor")

        // auto expand to show content
        this.addEventListener("input", () => {
            if (this.clientHeight < this.scrollHeight) {
                this.style.height = this.scrollHeight + 10 + "px" // + 10 to ensure no scroll
            }
        })

        this.createButton("bold", () => {
            this.insertTagIntoArea("b")
        }),
            this.createButton("italics", () => {
                this.insertTagIntoArea("i")
            }),
            this.createButton("underline", () => {
                this.insertTagIntoArea("u")
            }),
            this.createButton("striketrough", () => {
                this.insertTagIntoArea("strike")
            }),
            this.createButton("inline-code", () => {
                this.insertTagIntoArea("c")
            }),
            this.createButton("spoiler", () => {
                this.insertTagIntoArea("spoiler")
            })
    }

    private createButton(title: string, onclick: Function) {
        this.wrapper.addPopupElement(
            createElement("button", {
                className: `bbcode-element-wrapper__settings-popup-button button button--secondary`,
                attributes: {
                    innerText: title,
                    onclick: () => {
                        onclick()
                    },
                },
            })
        )
    }

    public getBBCodeData(): TextAreaData {
        // TODO: ensure that this is not just bbcode tags without any string content
        return new TextAreaData(this.value)
    }

    private insertTagIntoArea(name: string, value?: string) {
        const selectionStart = this.selectionStart
        const selectionEnd = this.selectionEnd

        let text = ""

        if (selectionStart == selectionEnd) {
            text = `[${name}${value != undefined ? `=${value}` : ""}][/${name}]`
        } else {
            let selectedText = this.value.slice(selectionStart, selectionEnd)
            text = `[${name}${value != undefined ? `=${value}` : ""}]${selectedText}[/${name}]`
        }

        const cursorPosition = this.value.substring(0, selectionEnd).length + text.length

        // value setting
        this.value = this.value.substring(0, selectionStart) + text + this.value.substring(selectionEnd, this.value.length)
        // update event + set cursor focus
        this.dispatchEvent(new Event("input", { bubbles: true }))
        this.focus()
        this.selectionEnd = cursorPosition
        this.selectionStart = cursorPosition
    }
}

customElements.define("bbcode-textarea", TextArea, { extends: "textarea" })
